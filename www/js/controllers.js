angular.module('starter.controllers', ['starter.services'])

.controller('DashCtrl', function($scope, poseadaAPI, $ionicModal) {
  String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).reverse().join(replacement);
  };

  $scope.getNumber = function(num) {
    return new Array(num);   
  }

  $ionicModal.fromTemplateUrl('modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal = modal;
   });
  
   $scope.openModal = function() {
      $scope.modal.show();
   };
  
   $scope.closeModal = function() {
      $scope.modal.hide();
   };

  $scope.getResult = function(data) {
    poseadaAPI.getResult(data).then(function(data) {
      var temp = document.createElement('div');
      temp.innerHTML = data.data;
      var pizarron = temp.querySelector(".pizarron");
      var numeros = pizarron.querySelectorAll(".numeros td");
      numeros.forEach(function(el){ console.log(el.textContent)})
    });
  }

  $scope.games = 1;
  $scope.inputPoceadaNumbers = {}

  $scope.addGame = function() {
    $scope.games++;
  }

  poseadaAPI.getDates().then(function(data) {
      var arrResult = [];
      var temp = document.createElement('div');
      temp.innerHTML = data.data;
      var results = temp.querySelectorAll("#container .juegos div");
      results.forEach(function(el){
        arrResult.push({
          sorteoNro: el.childNodes[1].textContent.trim(),
          sorteoFecha: el.childNodes[3].textContent.trim(),
          sorteoFechaFormatted: el.childNodes[3].textContent.trim().toString().replaceAll("/","-")
        });
      });
      console.log(arrResult)
      $scope.dates = arrResult;
  }, function(err) {
    console.log(err);
  });
})

.controller('ChatsCtrl', function($scope) {
})

.controller('ChatDetailCtrl', function($scope) {
})

.controller('AccountCtrl', function($scope) {
});

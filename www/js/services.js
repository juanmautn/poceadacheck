angular.module('starter.services', [])

.service('poseadaAPI', function($http){

  getResult = function(data) {
    return $http({method: 'GET',
        url: '//loteria.chaco.gov.ar/index.php/extractos/verPoceada/' + data});
  }

  getDates = function() {
  	return $http({
  		method: 'GET',
  		url: 'http://loteria.chaco.gov.ar/index.php/extractos/poceada'});
  }
  
  return {
    getResult : getResult,
    getDates  : getDates
  };
});
